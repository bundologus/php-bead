<?php
require_once("_init.php");
authorize(NOT_LOGGED_IN);
$user_lib = new UserStorage();
$user_lib->userLogin();
$errors = $user_lib->getErrors();
?>

<?php require("partials/header.php");?>
<main class="container-fluid">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <form id="register" method="post" action="login.php">
                <h1>Bejelentkezés</h1>
                <?php if(count($errors)) : ?>
                    <?php if(array_key_exists("userDataError",$errors)) : ?> 
                    <div class="alert alert-danger"><strong>Hoppá!</strong> A megadott email cím-jelszó páros érvénytelen.</div>
                    <?php endif; ?>
                    <?php $user_lib->clearAllErrors(); ?>
                <?php endif; ?>
                <div class="form-group">
                    <label class="control-label" for="email">E-mail:</label>
                    <input type="text" class="form-control" name="email" placeholder="peldajani@mailinator.net" required>
                </div>
                <div class="form-group">
                    <label class="control-label" for="password">Jelszó:</label>
                    <input type="password" class="form-control" name="password" required>
                </div>
                <button type="submit" class="btn btn-success">Bejelentkezek</button>
            </form>
        </div>
        <div class="col-md-3"></div>
    </div>
</main>
<script src="js/emailChecker.js" type="module"></script>
<?php require("partials/footer.php"); ?>