<?php
require_once("../_init.php");
header("Content-type: application/json");

$game_storage = new FileStorage("../storage/games.json");
$target = $_GET["target"];
$gameId = $_GET["gameId"];
$game_data = $game_storage->getdata();
$msg = ["error"];
if($target === "get") {
    foreach($game_data as $game) {
        if($game->id === $gameId) {
            $msg = $game;
        }
    }
} elseif($target === "delete") {
    $key;
    foreach($game_data as $game) {
        if($game->id === $gameId) {
            $key = array_search($game, $game_data);
        }
    }
    if(isset($key)){
        $game_storage->delete($key);
        $msg = ["ok"];
    }
} elseif($target === "solved") {
    foreach($game_data as $game) {
        if($game->id === $gameId) {
            $game->solved += 1;
            $msg = ["ok", "solved increased to " . $game->solved ];
        }
    }
}
    
$string = json_encode($msg);
print_r($string);