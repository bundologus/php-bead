<?php
require_once("../_init.php");
header("Content-type: application/json");

$save_storage = new SaveStorage("../storage/");
$target = $_GET["target"];
$value = $_GET["value"];
$msg = ["error"];
if($target === "solved" ) {
    $solved = $save_storage->getSolved();
    if($value !== "" && !in_array($value, $solved)) {
        $save_storage->addToSolved($value);
        $msg = $save_storage->getSolved();
    }
    $msg = ["ok"];
} elseif($target === "saves") {
    $data =$_GET["data"];
    $save_storage->addToSaves($value, $data);
    $msg=["ok"];
} elseif($target === "getSaves") {
    $saves = $save_storage->getSaves();
    if(array_key_exists($value,$saves)) {
        $msg = $saves[$value];
    } else {
        $msg = ["No saves found"];
    }
}

$string = json_encode($msg);
print_r($string);