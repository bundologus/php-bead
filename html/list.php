<?php require_once("_init.php"); ?>
<?php require("partials/header.php"); ?>
<?php
authorize(LOGGED_IN);
$games_storage = new FileStorage("storage/games.json");
$saves_storage = new SaveStorage();
?>
<main class="container-fluid">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <?php if(is_logged_in()) : ?>
            <div class="panel panel-default">
                <div class="panel-heading"><strong>További játékok</strong></div>
                <div class="panel-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="table-column-1">Név</th>
                                <th class="text-center">Nehézség</th>
                                <th class="text-center">Megoldották</th>
                                <th class="text-center">Megoldottad</th>
                                <?php if($_SESSION["userId"] === 0) : ?>
                                <th class="text-center">Törlés</th>
                                <?php endif; ?>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($games_storage->getdata() as $game) : ?>
                            <tr id=<?= $game->id ?>>
                                <td class="table-column-1"><?= $game->name ?></td>
                                <td class="text-center"><?= $game->difficulty ?></td>
                                <td class="text-center"><?= $game->solved ?></td>
                                <td class="text-center"><?php if(in_array($game->id, $saves_storage->getSolved())) {print("&#9989;");} ?></td>
                                <?php if($_SESSION["userId"] === 0) : ?>
                                <td class="text-center"><button class="btn btn-xs del-btn">&#9851;</button></td>
                                <?php endif; ?>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php endif; ?>
        </div>
        <div class="col-md-3"></div>
    </div>
    <div class="row modal hidden" id="modal">
</main>
<script src="js/list.js" type="module"></script>
<?php require("partials/footer.php"); ?>