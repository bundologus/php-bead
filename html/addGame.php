<?php
require_once("_init.php");
authorize(LOGGED_IN);
$errors = [];
if($_SESSION["userId"] === 0) {
    if(array_all_keys_exist($_POST, "name", "difficulty", "mtx")) {
        $game_storage = new FileStorage("storage/games.json");
        $games = $game_storage->getdata();
        $rawMtx = $_POST["mtx"];
        $mtx = json_decode("[" . $rawMtx . "]");
        

        $new = [
            "id" => md5($rawMtx),
            "name" => $_POST["name"],
            "difficulty" => $_POST["difficulty"],
            "mtx" => ($mtx),
            "solved" => 0
        ];
        $match = FALSE;
        foreach($games as $game) {
            if($game->id === $new["id"]) {
                $match = TRUE;
                $errors["gameAlreadyExistsError"] = $game->name;
            };
        }
        if(!$match) {
            $obj = (object) $new;
            $game_storage->add($obj);
        }
    }
}
?>

<?php require("partials/header.php");?>
<main class="container-fluid">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <?php if($_SESSION["userId"] === 0) : ?>
            <form id="register" method="post" action="addGame.php">
                <h1>Új játék hozzáadása</h1>
                <?php if(count($errors)) : ?>
                    <?php if(array_key_exists("gameAlreadyExistsError",$errors)) : ?> 
                    <div class="alert alert-danger"><strong>Hoppá!</strong> Ez a tábla már létezik <strong>"<?= $errors["gameAlreadyExistsError"] ?>"</strong> néven.</div>
                    <?php endif; ?>
                    <?php $errors = []; ?>
                <?php endif; ?>
                <div class="form-group">
                    <label class="control-label" for="name">Név:</label>
                    <input type="text" class="form-control" name="name" required>
                </div>
                <div class="form-group">
                    <label class="control-label" for="difficulty">Nehézség:</label>
                    <input type="number" min="1" max="4" value="1" class="form-control" name="difficulty" required>
                </div>
                <div class="alert alert-info small" style="padding: 5px;">
                    Az első beadandónál meghozott design döntések miatt a játék csak NxN-es mátrixokat tud kirajzolni. A feladatkiírás értelmében ezt itt már nem javítottam.
                </div>
                <div class="form-group">
                    <label class="control-label" for="mtx">Cellák:</label>
                    <textarea rows=10 form="register" class="form-control" name="mtx" placeholder="[2,1,2],&#10;[0,1,0],&#10;[0,0,0]" required></textarea>
                </div>
                <div class="button-group">
                    <button type="submit" class="btn btn-success pull-right">Feltöltés</button>
                </div>
            </form>
            <?php else : ?>
            <div class="alert alert-info">Sajnos ez a tartalom a számodra nem elérhető. :(</div>
            <?php endif; ?>
        </div>
        <div class="col-md-3"></div>
    </div>
</main>
<script src="js/addGame.js" type="module"></script>
<?php require("partials/footer.php"); ?>