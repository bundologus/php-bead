<?php
require_once("_init.php");
authorize(NOT_LOGGED_IN);
$user_lib = new UserStorage();
$user_lib->registerUser();
$errors = $user_lib->getErrors();
?>

<?php require("partials/header.php");?>
<main class="container-fluid">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <form id="register" method="post" action="register.php">
                <h1>Regisztráció</h1>
                <?php if(count($errors)) : ?>
                    <div class="alert alert-danger">
                        <strong>Hoppá!</strong> <?php if(count($errors)===1) {print("A következő");} else {print("Néhány");} ?> problémába ütköztünk:
                        <ul>
                        <?php if(array_key_exists("emailFormatError",$errors)) : ?>
                            <li>Érvénytelen email cím.</li>
                        <?php elseif(array_key_exists("emailTakenError",$errors)) : ?>
                            <li>Ezzel az email címmel már regisztráltak.</li>
                        <?php endif; ?>
                        <?php if(array_key_exists("usernameFormatError",$errors)) : ?>
                            <li>A felhasználónév legalább 2 karakter hosszú kell, hogy legyen.</li>
                        <?php elseif(array_key_exists("usernameTakenError",$errors)) : ?>
                            <li>Ezzel az felhasználónévvel már regisztráltak.</li>
                        <?php endif; ?>
                        <?php if(array_key_exists("pwFormatError",$errors)) : ?>
                            <li>A jelszónak legalább 8 karakter hosszúnak kell lennie.</li>
                        <?php endif; ?>
                        <?php $user_lib->clearAllErrors(); ?>
                    </div>
                <?php endif; ?>
                <div class="form-group">
                    <label class="control-label" for="email">E-mail:</label>
                    <input type="text" class="form-control" name="email" placeholder="peldajani@mailinator.net" required>
                </div>
                <div class="form-group">
                    <label class="control-label" for="username">Felhasználónév:</label>
                    <input type="text" class="form-control" name="username" placeholder="janiajani" required>
                </div>
                <div class="form-group">
                    <label class="control-label" for="password">Jelszó:</label>
                    <input type="password" class="form-control" name="password" placeholder="12345678" required>
                </div>
                <button type="submit" class="btn btn-success">Regisztrálok</button>
            </form>
        </div>
        <div class="col-md-3"></div>
    </div>
</main>
<script src="js/emailChecker.js" type="module"></script>
<?php require("partials/footer.php"); ?>