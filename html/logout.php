<?php
require_once("_init.php");
authorize(LOGGED_IN);
unset($_SESSION["user"]);
unset($_SESSION["userId"]);
redirect("index.php");