<?php require("_init.php"); ?>
<?php require("partials/header.php"); ?>
<main class="container-fluid">
  <div class="row">
    <div class="col-sm-2">
      <div class="panel panel-default">
        <div class="panel-heading"><strong>Próbajátékok</strong></div>
        <div class="panel-body">
          <div class="btn-group-vertical btn-group-md center-block" id="egGames">
            <button type="button" class="btn btn-primary" id="easy">Könnyű</button>
            <button type="button" class="btn btn-primary" id="medium">Közepes</button>
            <button type="button" class="btn btn-primary" id="hard">Nehéz</button>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-8">
      <h2> A játék leírása </h2>
      <blockquote>
        <p>Hol volt, hol nem volt, volt egyszer egy király. Ez a király olyan hatalmas volt, hogy ha eltüsszentette magát, az egész ország népének rá kellett mondani: adj’ Isten egészségére! Hogyha náthás volt, nem is lehetett más szót hallani az országban, mint: “Adj’ Isten egészségére!”</p>
      </blockquote>
      <p>A csillagszemű juhász még gondolat sem volt szülei fejében, mikor Nekeresdországban már az volt a szokás, hogy ha a király tüsszent, akkor mindenkinek jó egészséget kell kívánnia neki. Az egyszerű elv megvalósítása azonban komoly fejtörést okozott. A kivitelezésével Furfangot, a főkamarást bízták meg. Főtt is a főkamarás feje, mert úgy kellett a küldöncök útvonalát megtervezni, hogy a kijelölt várakból a küldöncök elindulva, egymás útját nem keresztezve, nem érintve, az egész birodalmat bejárva jussanak el a kijelölt célba. Segíts Furfangnak a küldöncök útvonalának megtervezésében!</p>
      <p>Nekeresdország egy négyzetráccsal ábrázolható. Van benne pár kitüntetett cella, ahonnan a küldöncök indulnak és ahova érkeznek. Egy-egy küldönc útvonalának végpontjait ugyanaz a szám jelzi. A küldönc bármelyik végpontból elindulhat. A küldöncök útvonalait az alábbi szabályok betartásával kell megtervezni:</p>
      <ul>
        <li>Az azonos számokat folytonos útvonallal kell összekötni.</li>
        <li>Az útvonalak csak vízszintesen és függőlegesen haladhatnak úgy, hogy a mezők közepét érinteniük kell, de ott 90 fokkal elfordulhatnak.</li>
        <li>Az útvonalak nem keresztezhetik egymást és nem ágazhatnak el.</li>
        <li>Az útvonalak számozott mezőkön nem mehetnek keresztül, viszont az összes fehér mezőn útvonalnak kell áthaladnia.</li>
      </ul>
    </div>
    <div class="col-sm-2">
    </div>
  </div>
  <div class="row modal hidden" id="modal">
  </div>
</main>
<script src="js/index.js" type="module"></script>
<?php require("partials/footer.php"); ?>