<?php
require("_init.php");
require("partials/header.php");
?>
<main class="container-fluid">
    <div class="row" id="game-row" data-game-id="<?= $_POST["gameId"] ?>"></div>
</main>
<script src="js/gameBuilder.js" type="module"></script>
<?php require("partials/footer.php"); ?>