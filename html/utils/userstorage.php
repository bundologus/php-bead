<?php
require_once("filestorage.php");
require_once("utils.php");

class UserStorage extends FileStorage {
    public function __construct() {
        parent::__construct("storage/users.json");
    }

    public function registerUser() {
        if(verify_post("email","username","password")) {
            $email = trim($_POST["email"]);
            $username = trim($_POST["username"]);
            $password = $_POST["password"];

            if(!$this->verifyEmail($email)) {
                $this->errors["emailFormatError"] = true;
            }

            if($this->userExists("",$email)) {
                $this->errors["emailTakenError"] = true;
            }
            
            if(!$this->verifyUsername($username)) {
                $this->errors["usernameFormatError"] = true;
            }
            
            if($this->userExists($username)) {
                $this->errors["usernameTakenError"] = true;
            }
            
            if(!$this->verifyPasswd($password)) {
                $this->errors["pwFormatError"] = true;
            }
            
            if(count($this->errors)===0) {
                $user = [
                    "email" => $email,
                    "username" => $username,
                    "password" => password_hash($password, PASSWORD_DEFAULT)
                ];
                
                $this->add($user);
                redirect("index.php");
            }
        }
    }

    public function userLogin() {
        if(verify_post("email","password")) {
            $email = trim($_POST["email"]);
            $password = $_POST["password"];

            if($this->verifyEmail($email)) {
                $user = $this->getUserByEmail($email);
                if(!empty($user)) {
                    $pwHash = $user->password;
                    $username = $user->username;
                    if(password_verify($password, $pwHash)) {
                        $_SESSION["user"] = $username;
                        $_SESSION["userId"] = $this->getUserId($username);
                        redirect("list.php");
                    } else {
                        $this->errors["userDataError"] = true;
                    }
                } else {
                    $this->errors["userDataError"] = true;
                }
            }

        }
    }

    public function verifyEmail($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    public function verifyPasswd($pw){
        return strlen($pw) >= 8;
    }

    public function verifyUsername($username) {
        return strlen($username) >= 2;
    }

    public function userExists($username="", $email="") {
        foreach($this->data as $entry) {
            if($entry->username === $username || $entry->email === $email) {
                return true;
            }
        }
        return false;
    }

    private function getUserByName($username) {
        foreach($this->data as $entry) {
            if($entry->username === $username) {
                return $entry;
            }
        }
        return [];
    }

    private function getUserByEmail($email) {
        foreach($this->data as $entry) {
            if($entry->email === $email) {
                return $entry;
            }
        }
        return [];
    }

    public function getKey($value) {
        $keys = array_keys($this->data);
        foreach($keys as $key) {
            if($this->data[$key]->username === $value) {
                return $key;
            }
        }
        return null;
    }

    public function getUserId($username) {
        return $this->getKey($username);
    }
}