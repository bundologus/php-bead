<?php

class FileStorage {
    protected $path;
    protected $data;
    protected $errors;

    public function __construct($path) {
        if (!file_exists($path)) {
            $file = fopen($path, "w+");
            fwrite($file, "[]");
            fclose($file);
        }

        if (!is_readable($path) || !is_writable($path)) {
            if (!chmod($path, 0666)) {
                throw new Exception("Insufficient permissions to read/write storage at $path.");
            }
        }

        $this->path = realpath($path);
        $this->data = (array) json_decode(file_get_contents(($this->path), TRUE));
        $this->errors = [];
    }

    public function __destruct() {
        $raw_data = json_encode($this->data, JSON_PRETTY_PRINT);
        file_put_contents($this->path, $raw_data);
    }

    public function add($value) {
        $this->data[] = $value;
    }

    public function getdata() {
        return $this->data;
    }

    public function find($id) {
        return $this->data[$id];
    }

    public function delete($id) {
        unset($this->data[$id]);
    }
  
    public function update($id, $new_value) {
        $this->data[$id] = $new_value;
    }

    public function clearAllErrors() {
        $this->errors = [];
    }

    public function clearError($errorType) {
        unset($this->errors[$errorType]);
    }

    public function getErrors() {
        return $this->errors;
    }

    public function getKey($value) {
        $keys = array_keys($this->data);
        foreach($keys as $key) {
            if($this->data[$key] === $value) {
                return $key;
            }
        }
        return null;
    }
}