<?php
require_once("filestorage.php");
require_once("utils.php");

class SaveStorage extends FileStorage {
    private $saves;
    private $solved; 
    public function __construct($relative_path="storage/") {
        $save_path = $relative_path .  $_SESSION["userId"] . "_saves.json";
        parent::__construct($save_path);
        if($this->data === []){
            $this->data["solved"] = [];
            $this->data["saves"] = [];
        }
        $this->solved = $this->data["solved"];
        $this->saves = (array) $this->data["saves"];
    }

    public function __destruct() {
        $this->data["solved"] = $this->solved;
        $this->data["saves"] = $this->saves;
        parent::__destruct();
    }

    public function getSolved() {
        return $this->solved;
    }
    public function getSaves() {
        return (array) $this->saves;
    }
    public function addToSolved($id) {
        $this->solved[] = $id;
    }
    public function addToSaves($id, $value) {
        if(!array_key_exists($id, $this->saves)) {
            $this->saves[$id] = [];
        }
        array_push($this->saves[$id], $value);
    }
}