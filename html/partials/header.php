<!DOCTYPE html>
<?php require_once("_init.php"); ?>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="./css/style-overrides.css">
  <title>Küldöncök</title>
</head>
<body>
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="index.php">Logo</a>
      </div>
      <ul class="nav navbar-nav">
        <?php if(is_logged_in()) : ?>
        <li class="navbar-text">Üdv, <?php print_r($_SESSION["user"]); ?>!</li>
        <li><a class="nav-link" href="list.php">Játék lista</a></li>
        <?php if($_SESSION["userId"]===0) : ?>
            <li><a class="nav-link" href="addGame.php">Játék hozzáadása</a></li>
        <?php endif; ?>
        <li><a class="nav-link" href="logout.php">Kijelentkezés</a></li>
        <?php else : ?>
        <li><a class="nav-link" href="login.php">Bejelentkezés</a></li>
        <li><a class="nav-link" href="register.php">Regisztráció</a></li>
        <?php endif; ?>
      </ul>
    </div>
  </nav>