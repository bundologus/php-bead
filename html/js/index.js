import { Game } from "./displayables/game.js";
import { gameLib } from "./lib/gameLib.js";

function launchGame(e) {
    let btn = e.target;
    let lvl = btn.id;
    let mtx = gameLib(lvl);
    let modal = document.querySelector(".modal");
    let game = new Game(mtx, "free-" + lvl);
    game.makeHeader(btn.innerText);
    game.displayIn(modal);
    modal.classList.toggle("hidden", false);
    game.buildField();
    let heading = document.querySelector("#game-wrap > .panel-heading");
    game.saveTab.displayIn(heading);
} 

const btns = document.getElementById("egGames");
btns.addEventListener("click", launchGame);