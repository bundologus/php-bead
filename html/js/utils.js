/**General utility functions the app uses.
 * @function handleGameBtn Generates and a game instance and displays it while hiding the menu.
 * @function handleDotClick Fires when the mouse is pushed one of the tracking cells on a dot.
 *   also sets up all the necessary trackers for handling the rest of the inputs like mouseover, 
 */

export function getCoord(element) {
    return element.getAttribute("id").slice(0, 2);
}

export function getLine() {
    return document.getElementById("LiB");
}

export function backToMenu() {
    let modal = document.querySelector(".modal");
    let child = modal.lastElementChild;
    while(child) {
        modal.removeChild(child);
        child = modal.lastElementChild;
    }
    modal.classList.toggle("hidden", true);
}

export function displayNotice(text) {
    if (document.getElementById("notice")) {
        document.querySelector("body").removeChild(document.getElementById("notice"));
    }
    let note = document.createElement("div");
    note.classList = "panel panel-info notice";
    note.id = "notice";
    note.innerText = text;
    
    let cls = document.createElement("button");
    cls.classList = "btn btn-xs btn-primary note-close-btn";
    cls.innerHTML = "&#10005;";
    cls.addEventListener("click", function () {
        document.querySelector("body").removeChild(document.getElementById("notice"));
    });

    note.appendChild(cls);
    document.querySelector("body").appendChild(note);
}

export function redirect(url, paramName="", paramValue="") {
    let form = document.createElement("form");
    form.method = "post";
    form.action = url;
    form.classList = "hidden";
    if(paramName !== "" && paramValue!== "") {
        let input = document.createElement("input");
        input.type = "hidden";
        input.name = paramName;
        input.value = paramValue;
        form.appendChild(input);
    }
    document.body.appendChild(form);
    form.submit();
}

export function fixTime($time) {
    let parts = $time.split(/[ :]/);
    let fixed = parts[0]+" ";
    if(parts[1].length === 1) {
        fixed += "0";
    }
    fixed+=parts[1]+":";
    if(parts[2].length === 1) {
        fixed += "0";
    }
    fixed+=parts[2]+":";
    if(parts[3].length === 1) {
        fixed += "0";
    }
    fixed+=parts[3];
    return fixed;
}