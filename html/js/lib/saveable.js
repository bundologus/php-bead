export class Saveable {
    constructor (stored = null) {
        if (stored) {    
            let data = JSON.parse(stored);
            this.lines = data.lines;
            this.trackers = data.trackers;
        } else {
            this.lines = [];
            this.trackers = [];
        }
    }

    restore() {
        let oldLines = document.querySelectorAll("polyline");
        let svg = document.querySelector(".input-svg");
        for (let l of oldLines) {
            svg.removeChild(l);
        }

        for (let l of this.lines) {
            let line = document.createElementNS("http://www.w3.org/2000/svg", "polyline");
            line.setAttribute("data-coords", l.coords);
            line.setAttribute("points", l.points);
            line.setAttribute("stroke-width", l.strokeWidth);
            line.setAttribute("data-lineVal", l.lineVal);
            line.setAttribute("stroke", l.color);
            line.classList = "line";
            svg.insertBefore(line, svg.firstChild);
        }

        for (let t of this.trackers) {
            document.getElementById(t.id).setAttribute("data-line", t.line);
        }
    }

    collect() {
        let lines = document.querySelectorAll("polyline");
        for (let l of lines) {
            let line = {
                color: l.getAttribute("stroke"),
                coords: l.getAttribute("data-coords"),
                points: l.getAttribute("points"),
                strokeWidth: l.getAttribute("stroke-width"),
                lineVal: l.getAttribute("data-lineVal")
            };
            this.lines.push(line);
        }
        let trackers = document.querySelectorAll(".tracker");
        for (let t of trackers) {
            let tracker = {
                id: t.id,
                line: t.getAttribute("data-line")
            }
            this.trackers.push(tracker);
        }
    }
}