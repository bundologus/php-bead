import { backToMenu, displayNotice, redirect } from "../utils.js";
import { handleReset, initNoLineState } from "../displayables/game.js";
import { Saveable } from "./saveable.js"

export class SaveLoad {
    constructor() {
        this.panel = document.createElement("div");
        this.panel.id = "save-panel";
        
        let close = document.createElement("button");
        let save = document.createElement("button");
        let load = document.createElement("button");
        let reset = document.createElement("button");

        close.style.backgroundImage = "url(img/close.png)";
        save.style.backgroundImage = "url(img/save.png)";
        load.style.backgroundImage = "url(img/load.png)";
        reset.style.backgroundImage = "url(img/reset.png)";

        close.classList.add("sl-btn");
        save.classList.add("sl-btn");
        load.classList.add("sl-btn");
        reset.classList.add("sl-btn");

        save.addEventListener("click", handleSaveClick);
        load.addEventListener("click", handleLoadClick);
        reset.addEventListener("click", handleResetClick);
        if(window.location.href.includes("game.php")) {
            close.addEventListener("click", function() {
                redirect("list.php");
            });
        } else {
            close.addEventListener("click", backToMenu);
        }

        close.setAttribute("title", "Bezár");
        save.setAttribute("title", "Mentés");
        load.setAttribute("title", "Betöltés");
        reset.setAttribute("title", "Elölről");

        this.panel.appendChild(reset);
        if (typeof(Storage) !== "undefined" && !window.location.href.includes("game.php")) {
            this.panel.appendChild(save);
            this.panel.appendChild(load);
        }
        this.panel.appendChild(close);

    }

    displayIn(lmnt) {
        if (typeof(lmnt) !== "object" ) throw (new TypeError("lmnt must be an object!"));
        lmnt.appendChild(this.panel);
    }
}

function handleSaveClick() {
    let key = document.getElementById("game-wrap").getAttribute("data-level");
    if (window.localStorage.getItem(key) !== null) {
        let wrap = document.createElement("div");
        wrap.id = "warn-wrap";
        wrap.classList = "full-page-cover";

        let panel = document.createElement("div");
        panel.classList = "panel panel-warning save-confirm";

        let head = document.createElement("div");
        head.classList = "panel-heading";
        head.innerText = "Vigyázat!"

        let body = document.createElement("div");
        body.classList = "panel-body";

        let text = document.createElement("p");
        text.innerText = "Ezzel a paranccsal felülírod a korábban elmentett állást. Biztosan mentessz?"

        let btnFlex = document.createElement("div");
        btnFlex.classList = "btn-flex"; 

        let ok = document.createElement("button");
        ok.id = "ok-btn";
        ok.classList = "btn btn-warning";
        ok.innerText = "Igen";
        ok.addEventListener("click", function () {
            saveGame();
        });

        let cancel = document.createElement("button");
        cancel.id = "cancel-btn";
        cancel.classList = "btn btn-default";
        cancel.innerText = "Mégse";
        cancel.addEventListener("click", handleCancelClick);

        wrap.appendChild(panel);
        panel.appendChild(head);
        panel.appendChild(body);
        body.appendChild(text);
        body.appendChild(btnFlex);
        btnFlex.appendChild(ok);
        btnFlex.appendChild(cancel);

        document.getElementById("modal").appendChild(wrap);
    } else {
        saveGame();
    }
}

function saveGame() {
    let key = document.getElementById("game-wrap").getAttribute("data-level");
    let item = new Saveable();
    item.collect();
    let storable = JSON.stringify(item);

    window.localStorage.setItem(key, storable);
    handleCancelClick();
    displayNotice("Az állás elmentve.");
}

function handleLoadClick() {
    let key = document.getElementById("game-wrap").getAttribute("data-level");
    let storable = window.localStorage.getItem(key);
    if (storable !== null) {
        let loaded = new Saveable(storable); 
        loaded.restore();
        initNoLineState();
        displayNotice("Az állás betöltve.");
    } else {
        displayNotice("Ezen a gépen nincs betölthető mentés erre a szintre.");
    }
}

function handleResetClick() {
    handleReset();
}

function handleCancelClick() {
    document.getElementById("modal").removeChild(document.getElementById("warn-wrap"));
}