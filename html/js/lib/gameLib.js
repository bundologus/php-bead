/**Returns the matrix describing the map of a level.
 * May be expanded later to several functions for different levels,
 * with IDs as parameter to choose from multiple "easy", "medium" or "hard" levels.
 * May also be expanded to return level data from a DB.
 * @param lvl The difficulty or ID of the level to be loaded.
 */

export function gameLib(lvl) {
    if (lvl === "easy") {
        return [[0,0,0,2,0],
                [0,1,0,0,0],
                [0,0,2,0,0],
                [3,0,0,3,0],
                [1,0,0,0,0]];
    } else if (lvl === "medium") {
        return [[2,0,0,9,0,0,0,5,0],
                [1,0,0,8,0,11,0,0,5],
                [0,2,0,0,6,0,7,0,0],
                [0,0,0,0,0,11,0,10,0],
                [0,0,0,7,0,0,0,0,0],
                [0,0,0,4,0,0,0,0,0],
                [0,0,0,0,0,0,0,3,6],
                [0,9,0,4,8,0,0,0,0],
                [0,1,0,0,0,0,0,10,3]];
    } else if (lvl === "hard") {
        return [[1,0,0,0,3,0,5,0,2],
                [0,0,0,0,0,0,8,5,0],
                [7,4,0,6,0,0,0,0,0],
                [0,0,0,0,0,0,1,0,0],
                [0,0,0,0,0,0,0,0,2],
                [0,0,4,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0,0],
                [0,7,0,0,0,0,3,0,0],
                [0,0,0,6,0,0,0,0,8]];
    } else {
        throw Error("Invalid input");
    }
}