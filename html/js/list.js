import { redirect } from "./utils.js";

let rows = document.querySelectorAll("tbody > tr");
for(let row of rows) {
    row.addEventListener("click", function(e){
        let gameId = this.id;
        redirect("game.php", "gameId", gameId);
    });
}

let btns = document.querySelectorAll(".btn-xs");
for(let btn of btns) {
    btn.addEventListener("click", deleteGameHandler);
}

function deleteGameHandler(e) {
    e.stopPropagation();
    let gameId = e.target.parentElement.parentElement.id;
    let tr = document.getElementById(gameId);
    let lvl = tr.childNodes[1].innerText;

    let cnclBtn = document.createElement("button");
    cnclBtn.classList = "btn pull-right panel-close";
    cnclBtn.innerHTML = "&times;";

    let heading = document.createElement("div");
    heading.classList = "panel-heading lead";
    heading.innerText = "Vigyázat!"
    heading.appendChild(cnclBtn);
    heading.lastChild.addEventListener("click", closePanel);
    
    let cnfrmBtn = document.createElement("button");
    cnfrmBtn.classList = "btn btn-danger pull-right";
    cnfrmBtn.innerText = "Törlés!";

    let pbody = document.createElement("div");
    pbody.classList = "panel-body";
    pbody.innerHTML = `<p>Biztosan törölni akarod a(z) <strong>${lvl}</strong> pályát? Később ezt már nem tudod visszavonni!<p>`;
    pbody.appendChild(cnfrmBtn);
    pbody.lastChild.addEventListener("click", e => {confirmDelete(gameId);});
    
    let panel = document.createElement("div");
    panel.classList="panel panel-warning center-block";
    panel.appendChild(heading);
    panel.appendChild(pbody);

    let modal = document.getElementById("modal");
    modal.appendChild(panel);
    modal.classList.toggle("hidden", false);

}

async function confirmDelete(gameId) {
    let tr = document.getElementById(gameId);
    tr.classList.toggle("delete-this", true);
    let xhr = new XMLHttpRequest();
    xhr.addEventListener("load", responseHandler);
    xhr.open("get", `../api/gameApi.php?target=delete&gameId=${gameId}`);
    xhr.responseType = "json";
    xhr.send(null);
}

function closePanel() {
    let modal = document.getElementById("modal");
    let panel = document.querySelector(".panel-warning");
    modal.removeChild(panel);
    modal.classList.toggle("hidden", true);
}

function responseHandler() {
    let resp = this.response;
    let tr = document.querySelector("tr.delete-this");
    if(resp[0] === "ok") {
        closePanel();
        tr.parentElement.removeChild(tr);
    } else if(resp[0] === "error") {
        source.id = "";
    }
}