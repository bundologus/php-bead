/**The descendants of this class can be displayed on the screen as panels.
 * You can add a heading or footer with the supplied methods.
 * @param style Optional. Expects a class name. Changes the styling of the panel.
 * @param wrap The 'div' element that contains the menu.
 * @method displayIn Displays the menu inside the provided element.
 *   @param lmnt The element the menu should be the child of.
 * @method makeHeader Creates a header for the panel.
 *   @param text The test that will be displayed in the header.
 * @method makeFooter Creates a footer for the panel.
 *   @param text The test that will be displayed in the footer.
 */

export class Displayable {
    constructor(style="panel-default") {
        this.wrap = document.createElement("div");
        this.wrap.classList = "panel center-block " + style;
        this.wrap.style.paddingLeft = 0;
        this.wrap.style.paddingRight = 0;
        this.body = document.createElement("div");
        this.body.classList = "panel-body";
        this.wrap.appendChild(this.body);
        this.inDOM = false;
        
        this.header;
        this.footer;
    }

    displayIn(lmnt) {
        if(typeof(lmnt) !== "object" ) throw (new TypeError("lmnt must be an object!"));

        lmnt.appendChild(this.wrap);
        this.inDOM = true;
    }
    
    makeHeader(text) {
        this.header = document.createElement("div");
        this.header.classList = "panel-heading lead";
        this.header.style.marginBottom = 0;
        this.header.innerHTML = text;
        
        this.wrap.insertBefore(this.header, this.body);
    }
    
    makeFooter(text) {
        this.footer = document.createElement("div");
        this.header.classList.add("panel-footer");
        this.footer.innerText = text;
        
        this.wrap.appendChild(this.footer);
    }
}