/**The Menu object is responsible for generating and displaying the game menu.
 * It inherits all the parametes and methods of the Displayable class.
 * The caller may add buttons as necessary with the 'addGameButton' method.
 * @method addGameButton Generates and adds a button to the menu.
 *   @param text The display text of the button.
 *   @param target The ID of the level the button refers to.
 */

import { Game } from "./game.js";
import { Displayable } from "./displayable.js";

export class Menu extends Displayable{
    constructor() {
        super();
        this.wrap.id = "menu-wrap";
        this.gameBtns = document.createElement("div");
        this.gameBtns.classList = "btn-group-vertical btn-group-lg";
        this.gameBtns.id = "game-btns";
        this.body.appendChild(this.gameBtns);
    }

    addGameButton(text, target) {
        let btn = document.createElement("button");
        btn.classList.add("btn");
        btn.classList.add("btn-primary");
        btn.setAttribute("type", "button");
        btn.innerText = text;
        btn.setAttribute("trgt", target);
        btn.addEventListener("click", handleGameBtn);

        this.gameBtns.appendChild(btn);
    }
}

function handleGameBtn(e) {
    let target = e.target.getAttribute("trgt");
    let menuRow = document.getElementById("menu-row");
    let gameRow = document.getElementById("game-row");
    menuRow.classList.toggle("hidden");
    gameRow.classList.toggle("hidden");

    let game = new Game(target);
    game.makeHeader(e.target.innerText + " játék")
    
    game.displayIn(gameRow);
    game.buildField();
    game.saveTab.displayIn(gameRow);
}