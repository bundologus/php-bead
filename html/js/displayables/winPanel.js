
import { Displayable } from "./displayable.js";
import { backToMenu, redirect } from "../utils.js";

export class WinPanel extends Displayable{
    constructor(text) {
        super("panel-success");
        this.wrap.id = "winpanel-wrap";
        this.makeHeader("Gratulálok!");
        
        let h2 = document.createElement("h2");
        h2.innerText = "Teljesítetted a " + text + " szintet!";
        let btn = document.createElement("button");
        if(this.checkEnvIsIndex()) {
            btn.innerText = "Vissza a kezdőlapra";
            btn.addEventListener("click", backToMenu);
        } else {
            btn.innerText = "Vissza a listához";
            let gameWrap = document.getElementById("game-wrap");
            let gameId = gameWrap.getAttribute("data-level");
            this.recordWin(gameId);
            btn.addEventListener("click", function() {
                redirect("list.php");
            });
        }
        btn.classList = "btn btn-success center-block";
        
        this.body.appendChild(h2);
        this.body.appendChild(btn);
    }

    checkEnvIsIndex() {
        if(window.location.href.includes("index")){
            return true;
        }
        return false;
    }

    recordWin(value) {
        let xhr = new XMLHttpRequest();
        xhr.addEventListener('load', this.responseHandler);
        xhr.open("get", `api/storageApi.php?target=solved&value=${value}`);
        xhr.send(null);

        let xhr2 = new XMLHttpRequest();
        xhr2.addEventListener('load', this.responseHandler);
        xhr2.open("get", `api/gameApi.php?target=solved&gameId=${value}`);
        xhr2.send(null);
    }

    responseHandler() {
        let resp = this.responseText;
        console.log(resp);
    }
}