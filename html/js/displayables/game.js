/**The representation of an instance of the game.
 * Generates and displays the playing field, tracks input and displays game state.
 * Contains the level data, the current state, lets the user save and load.
 * Inherits all methods and parameters of the Displayable class.
 * @param mtx Contains the playing fields' data.
 * @param n The number of squares in a row or column of the plaing field.
 * @param sate The current state of the game.
 * @method buildField() Generates the playing field from mtx.
 * ! the game object must be added to the DOM before the field is generated.
 */

import { Displayable } from "./displayable.js";
import { getCoord, getLine } from "../utils.js";
import { WinPanel } from "./winPanel.js";
import { SaveLoad } from "../lib/saveLoad.js";

export class Game extends Displayable{
    constructor(levelMtx, gameID, free=true) {
        super();
        this.wrap.id = "game-wrap";
        this.mtx = levelMtx;
        this.n = this.mtx.length;
        this.field = document.createElement("div");
        this.field.classList.add("field");
        this.inputSvg = document.createElementNS("http://www.w3.org/2000/svg", "svg") ;
        this.inputSvg.classList.add("input-svg");
        this.saveTab = new SaveLoad();
        this.body.appendChild(this.field);
        this.body.appendChild(this.inputSvg);

        this.wrap.setAttribute("data-level", gameID);
        
        this.cells = [];
        
    }

    buildField() {
        if (!this.inDOM) {
            throw Error("The object must be added to the DOM before the field can be built.\nUse the displayIn() function first!");
        }
        
        // setting playing field dimensions and style attributes, generating grid dimensions based on input matrix
        // ! square matrix assumed
        let maxW = window.innerWidth - 90; //= minimum margin 30px + 30px + panel padding 15px + 15px
        let maxH = window.innerHeight - 198; //= bottom panel minHeight 25px + minimum margin 30px + 30px + panel padding 15px + 15px + panel header 43px
        let delta = maxW < maxH ? maxW / this.n : maxH / this.n;
        this.inputSvg.setAttribute("data-delta", delta); // needed for the manipulation of the line later on.
        let strPart = delta.toString() + "px";
        
        let fullStr = strPart;
        for (let i=1; i<this.n; ++i) {
            fullStr += " " + strPart;
        }

        this.field.style.gridTemplateColumns = fullStr;
        this.field.style.gridTemplateRows = fullStr;

        // generating cells for the game field and pushing them into the grid
        for (let y=0; y<this.n; ++y) {
            let row = [];
            this.cells.push(row);
            for (let x=0; x<this.n; ++x) {
                let cell = document.createElement("div");
                if (this.mtx[y][x] !== 0) {
                    cell.innerText = this.mtx[y][x];
                }
                cell.setAttribute("id", x.toString() + y.toString() + "cell");
                cell.style.height = strPart;
                cell.style.lineHeight = strPart;
                cell.classList.add("cell");
                if (y === this.n-1) {
                    cell.classList.add("cell-bottom");
                }
                if (x === this.n-1) {
                    cell.classList.add("cell-right");
                }
                this.cells[y].push(cell);
                this.field.appendChild(cell);
            }
        }

        // formatting corners
        this.cells[0][0].classList.add("cell-tl");
        this.cells[0][this.n-1].classList.add("cell-tr");
        this.cells[this.n-1][this.n-1].classList.add("cell-br");
        this.cells[this.n-1][0].classList.add("cell-bl");

        // formatting the input svg to cover the field
        let fieldStats = this.field.getBoundingClientRect();
        //this.inputSvg.style.top = fieldStats.top.toString() + "px";
        this.inputSvg.style.width = fieldStats.width.toString() + "px";
        this.inputSvg.style.height = fieldStats.height.toString() + "px";

        // generating mouseover tracking cells and colored dots on the svg
        for (let y=0; y<this.n; ++y) {
            for (let x=0; x<this.n; ++x) {
                // creating tracking cell
                let tracker = document.createElementNS("http://www.w3.org/2000/svg", "rect");
                tracker.setAttribute("x", x*delta);
                tracker.setAttribute("y", y*delta);
                tracker.setAttribute("width", delta);
                tracker.setAttribute("height", delta);
                tracker.classList.add("tracker");
                tracker.setAttribute("id", x.toString() + y.toString() + "tracker");
                tracker.setAttribute("data-centerx", (x + 0.5)*delta); // center coordinates for 
                tracker.setAttribute("data-centery", (y + 0.5)*delta); // easy line-drawing
                tracker.setAttribute("data-dot", "");
                tracker.setAttribute("data-line", "");

                // creating dot
                if (this.mtx[y][x] !== 0) {
                    let dot = document.createElementNS("http://www.w3.org/2000/svg", "circle");
                    dot.setAttribute("cx", (x+0.5)*delta); // delta * 0.5 is the first column's offset
                    dot.setAttribute("cy", (y+0.5)*delta);
                    dot.setAttribute("r", delta/4);
                    dot.classList.add("dot");
                    dot.setAttribute("id", x.toString() + y.toString() + "dot");
                    dot.setAttribute("data-val", this.mtx[y][x]);
                    dot.style.fill = "var(--color-" + this.mtx[y][x] + ")";
                    dot.style.stroke = "var(--color-" + this.mtx[y][x] + ")";
                    this.inputSvg.appendChild(dot);
                    tracker.setAttribute("data-dot", this.mtx[y][x]);
                }
                
                // appending tracker after the dot to ensure foreground position 
                this.inputSvg.appendChild(tracker);
            }
        }
        initNoLineState();
    }
}

function handleDotEnter(e) {
    let coord = getCoord(e.target);
    let dot = document.getElementById(coord + "dot");
    dot.classList.toggle("dot-hover", true);
}

function handleDotLeave(e) {
    let coord = getCoord(e.target);
    let dot = document.getElementById(coord + "dot");
    dot.classList.toggle("dot-hover", false);
}

function handleLineStart(e) {
    e.preventDefault();
    let lineStartX = e.target.getAttribute("data-centerx");
    let lineStartY = e.target.getAttribute("data-centery");
    let startCoord = getCoord(e.target);
    let val = e.target.getAttribute("data-dot");
    let dots = document.querySelectorAll("[data-val=\"" + val + "\"]");
    let color = dots[0].style.fill;
    
    let width =  parseInt(dots[0].getAttribute("r"))*2;
    let line = document.createElementNS("http://www.w3.org/2000/svg", "polyline")
    line.classList.add("line");
    line.setAttribute("id", "LiB"); // Line in Build
    line.setAttribute("data-coords", startCoord);
    line.setAttribute("points", lineStartX.toString() + " " + lineStartY.toString());
    line.setAttribute("stroke-width", width);
    line.setAttribute("data-lineVal", val);
    line.setAttribute("stroke", color);
    let svg = document.querySelector(".input-svg");
    svg.insertBefore(line, svg.firstChild);
    e.target.setAttribute("data-line", line.getAttribute("data-lineVal"));
    
    for (let d of dots) {
        d.classList.toggle("dot-hover", true);
    }

    initLineBuilderState(e);
    document.addEventListener("mouseup", handleBreakLine);
    initExtensionTrackers(getCoord(e.target), true);
}

function handleExtendLine(e) {
    let line = getLine();
    let points = line.getAttribute("points");
    let newPtX = e.target.getAttribute("data-centerx");
    let newPtY = e.target.getAttribute("data-centery");
    let cds = line.getAttribute("data-coords");

    line.setAttribute("data-coords", cds + " " +  getCoord(e.target));
    points += " " + newPtX + " " + newPtY;
    line.setAttribute("points", points);
    e.target.setAttribute("data-line", line.getAttribute("data-lineVal"));

    let val = line.getAttribute("data-lineVal");
    let dotContent = e.target.getAttribute("data-dot"); 
    if ( dotContent === val ) {
        // * on mouse-up, line is saved
        e.target.addEventListener("mouseup", handleLineSuccess);
    } else {
        e.target.removeEventListener("mouseup", handleLineSuccess);
    }

    initExtensionTrackers(getCoord(e.target));
}

function handleLineSuccess(e) {
    e.stopPropagation();
    let line = getLine();
    let endTracker = e.target;
    let startTracker = document.getElementById(line.getAttribute("data-coords").slice(0, 2) + "tracker");
    let points = line.getAttribute("points");
    let cds = line.getAttribute("data-coords");
    let dots = document.querySelectorAll("[data-val=\"" + line.getAttribute("data-lineVal") + "\"]");
    let trackers = document.querySelectorAll(".tracker");
    let val = line.getAttribute("data-lineVal");

    points += " " + endTracker.getAttribute("data-centerx") + " " + endTracker.getAttribute("data-centery");
    line.setAttribute("points", points);
    line.setAttribute("id", "");
    line.setAttribute("data-coords", cds + " " +  getCoord(e.target));
    
    endTracker.removeEventListener("mousedown", handleLineStart);
    startTracker.removeEventListener("mousedown", handleLineStart);
    endTracker.setAttribute("data-line", line.getAttribute("data-lineVal"));

    for (let t of trackers) {
        t.removeEventListener("mouseenter", handleExtendLine);
        t.removeEventListener("mouseenter", handleBreakLine);
    }

    for (let d of dots) {
        d.classList.toggle("dot-hover", false);
    }

    document.removeEventListener("mouseup", handleBreakLine);
    document.removeEventListener("mouseup", handleLineSuccess);
    initNoLineState();
}

function handleBreakLine() {
    let line = getLine();
    let val = line.getAttribute("data-lineVal");
    let dots = document.querySelectorAll("[data-val=\"" + val + "\"]");
   
    removeLine(val);

    for (let d of dots) {
        d.classList.toggle("dot-hover", false);
    }

    document.removeEventListener("mouseup", handleBreakLine);

    initNoLineState();
}

function handleLineTrackBack(e) {
    // save line points data on tracker, and add handleLineTrackBack listener
    let line = getLine();
    let newEndCoord = getCoord(e.target);
    let coords = line.getAttribute("data-coords");
    let cds = coords.slice(3 + coords.indexOf(newEndCoord)).split(" ");
    let points = line.getAttribute("points");
    let pts = points.split(" ");
    
    //  for each coord in list after event target -> find tracker
    for (let c of cds) {
        let tracker = document.getElementById(c + "tracker");
        //      remove trackback event listener
        tracker.removeEventListener("mouseenter", handleLineTrackBack);
        //      delete data-line content
        tracker.setAttribute("data-line", "");
    }
    e.target.removeEventListener("mouseenter", handleLineTrackBack)
    
    //  shorten data-coord on line
    let newCoords = coords.slice(0, coords.indexOf(newEndCoord) + 2);
    line.setAttribute("data-coords", newCoords);

    //  shorten points on line
    let newPts = pts.slice(0, pts.length - 2*cds.length);
    line.setAttribute("points", newPts.join(" "));

    // set clear listeners
    let val = line.getAttribute("data-lineVal");
    let dotContent = e.target.getAttribute("data-dot"); 
    if ( dotContent === val ) {
        // * on mouse-up, line is saved
        e.target.addEventListener("mouseup", handleLineSuccess);
        document.removeEventListener("mouseup", handleBreakLine);
    } else {
        document.addEventListener("mouseup", handleBreakLine);
        e.target.removeEventListener("mouseup", handleLineSuccess);
    }
    // set up new listeners
    initExtensionTrackers(getCoord(e.target));
}

function handleDeleteLine(e) {
    let val = e.target.getAttribute("data-line");

    removeLine(val);
}

function removeLine(val) {
    /**Combined eventlistener remover for broken line and removed line.
     * Also removes the line from the DOM and reinitializes endpoints.
    */
    let line = document.querySelector("[data-lineVal=\"" + val + "\"]");
    let path = document.querySelectorAll("rect[data-line=\"" + val + "\"]");
    let trackers = document.querySelectorAll(".tracker");
    
    for (let p of path) {
        p.setAttribute("data-line", "");
        if (p.getAttribute("data-dot") === val) {
            p.removeEventListener("mousedown", handleDeleteLine);
        }
    }

    for (let t of trackers) {
        t.removeEventListener("mouseup", handleLineSuccess);
        t.removeEventListener("mouseenter", handleExtendLine);
        t.removeEventListener("mouseenter", handleBreakLine);
    }
    
    line.parentElement.removeChild(line);

    initNoLineState();
}

export function initNoLineState() {
    /**Initializes listeners for when there's no line in build.
     * Also checks for win conditions.
     */
    let win = true;
    let trackers = document.querySelectorAll(".tracker");
    for (let tracker of trackers) {
        if (tracker.getAttribute("data-dot") !== "") {
            if (tracker.getAttribute("data-line") === "") {
                tracker.addEventListener("mousedown", handleLineStart);
                tracker.removeEventListener("mousedown", handleDeleteLine);
            } else {
                tracker.removeEventListener("mousedown", handleLineStart);
                tracker.addEventListener("mousedown", handleDeleteLine);
            }
            tracker.addEventListener("mouseenter", handleDotEnter);
            tracker.addEventListener("mouseleave", handleDotLeave);
        }
        tracker.removeEventListener("mouseenter", handleLineTrackBack);
        win = win && (tracker.getAttribute("data-line") !== "");
    }

    // check win condition and build/show win panel if true
    if (win) {
        let lvl = document.querySelector("#game-wrap .panel-heading").innerText;
        let wp = new WinPanel(lvl);
        wp.displayIn(document.getElementById("game-wrap"));
    }
}

function initLineBuilderState() {
    /**Removes listeners of "NoLineState"*/

    let trackers = document.querySelectorAll(".tracker");
    for (let tracker of trackers) {
        if (tracker.getAttribute("data-dot") !== "") {
            tracker.removeEventListener("mousedown", handleLineStart);
            tracker.removeEventListener("mousedown", handleDeleteLine);
            tracker.removeEventListener("mouseenter", handleDotEnter);
            tracker.removeEventListener("mouseleave", handleDotLeave);
        }
    }
}

function initExtensionTrackers(coord, init=false) {
    /**Sets up input trackers on the cells surrounding the provided coordinate.
     * Removes previous extension trackers if init===false.
     */

    let x = parseInt(coord.slice(0,1));
    let y = parseInt(coord.slice(1));
    let val = getLine().getAttribute("data-lineVal");
    let endTrackers = document.querySelectorAll("[data-dot=\"" + val + "\"]");
    let finished = true
    for (let et of endTrackers) {
        if (et.getAttribute("data-line") === "") {
            finished = finished && false;
        }
    }

    let dcs = [ [ 0,-1], // a       _
                [-1, 0],  // b     _|a|_
                [ 1, 0],  // c    |b|_|c|
                [ 0, 1]]; // d      |d|
     
    if (!init) {
        let trackers = document.querySelectorAll(".tracker");
        for (let t of trackers) {
            t.removeEventListener("mouseenter", handleExtendLine);
            t.removeEventListener("mouseenter", handleBreakLine);
        }
    }
    
    for (let i=0; i< 4; ++i) {
        let trgt = document.getElementById((x+dcs[i][0]).toString() + (y+dcs[i][1]).toString() + "tracker");
        if (trgt !== null) {
            let dotContent = trgt.getAttribute("data-dot");
            let lineContent = trgt.getAttribute("data-line");
            if (!finished && lineContent === "" && (dotContent === "" || dotContent === val)) {
                // * the tracker extends
                trgt.addEventListener("mouseenter", handleExtendLine);
            } else if ( lineContent === val ) {
                // * the tracker backtracks
                trgt.addEventListener("mouseenter", handleLineTrackBack);
            }
        }
    }
}

export function handleReset() {
    
    let lines = document.querySelectorAll("polyline");
    let trackers = document.querySelectorAll(".tracker");

    for (let l of lines) {
        l.parentElement.removeChild(l);
    }

    for (let t of trackers) {
        t.setAttribute("data-line", "");
        t.removeEventListener("mousedown", handleDeleteLine);
    }

    initNoLineState();
}