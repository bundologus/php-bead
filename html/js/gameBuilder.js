import { Game, initNoLineState } from "./displayables/game.js";
import { Saveable } from "./lib/saveable.js";
import { fixTime, displayNotice } from "./utils.js";
let row = document.getElementById("game-row");
let lvl = row.getAttribute("data-game-id");
getGameData(lvl);

async function getGameData(lvl) {
    let xhr = new XMLHttpRequest();
    xhr.addEventListener('load', gameDataResponseHandler);
    xhr.open("GET", `api/gameApi.php?target=get&gameId=${lvl}`);
    xhr.responseType = "json";
    xhr.send(null);
}

function gameDataResponseHandler() {
    let lc = document.createElement("div");
    lc.classList = "col-md-3";
    row.appendChild(lc);

    let md = document.createElement("div");
    md.classList = "col-md-6";
    row.appendChild(md);

    let gameData = this.response;
    let game = new Game(gameData.mtx, gameData.id);
    game.makeHeader(gameData.name);
    game.displayIn(md);
    game.buildField();
    let heading = document.querySelector("#game-wrap > .panel-heading");
    game.saveTab.displayIn(heading);

    let rc = document.createElement("div");
    rc.classList = "col-md-3";
    row.appendChild(rc);
    
    getSavesData().then( response => response.json()).then( response => {
        buildSaveList(response);
    });
}

function getSavesData() {
    return fetch(`api/storageApi.php?target=getSaves&value=${lvl}`);
}

function buildSaveList($data) {
    let saves = $data;
    
    let saveBtn = document.createElement("button");
    saveBtn.innerText = "Mentés";
    saveBtn.classList = "btn btn-default center-block";
    saveBtn.addEventListener("click", saveGame);

    let par = document.createElement("p");
    par.innerText="Betöltéshez kattints valamelyik bejegyzésre.";
    
    let ref = document.querySelector("#game-wrap > .panel-body");
    let refH = parseInt(window.getComputedStyle(ref).height.slice(0,-2));
    let maxH = "" + (refH - 224) + "px";
    let ul = document.createElement("ul");
    ul.id = "saves";
    ul.classList = "well well-sm";
    ul.style.maxHeight = maxH;
    ul.addEventListener("mouseup", loadGame);

    if(saves[0] !== "No saves found"){
        for (let save of saves) {
            save = JSON.parse(save);
            let li = document.createElement("li");
            li.innerHTML="<a>"+save.timeStamp+"</a>";
            ul.appendChild(li);
        }
    }

    let info = document.createElement("div");
    info.classList = "alert alert-info small";
    info.innerText = "Az első beadandónál meghozott design döntések miatt a más képernyő méreten elmentett állások nem töltenek be rendesen. A feladatkiírás értelmében ezt itt már nem javítottam.";
    info.style.maxHeight = "100px";
    info.style.padding = "5px";

    let pbody = document.createElement("div");
    pbody.classList = "panel-body";
    pbody.appendChild(saveBtn);
    pbody.appendChild(par);
    pbody.appendChild(ul);
    pbody.appendChild(info);
    
    let heading = document.createElement("div");
    heading.classList="panel-heading lead";
    heading.innerText="Mentés/betöltés";
    
    let panel = document.createElement("div");
    panel.classList="panel panel-default";
    panel.appendChild(heading);
    panel.appendChild(pbody);
    
    let col = row.firstChild;
    col.appendChild(panel);
}

async function saveGame() {
    let today = new Date();
    let timeStamp = fixTime(""+today.getFullYear()+"\/"+(today.getMonth() + 1)+"\/"+today.getDay()+" "+today.getHours()+":"+today.getMinutes()+":"+today.getSeconds());
    let saver = new Saveable();
    saver.collect();
    let saveData = JSON.stringify(saver);
    let save = JSON.stringify({
        "timeStamp": timeStamp,
        "data": saveData
    });
    let xhr = new XMLHttpRequest();
    xhr.addEventListener('load', saveGameResponseHanlder)
    xhr.open("get", `api/storageApi.php?target=saves&value=${lvl}&data=${save}`);
    xhr.responseType = "json";
    xhr.send(null);

    let ul = document.getElementById("saves");
    let li = document.createElement("li");
    li.innerHTML = "<a>"+timeStamp+"</a>";
    li.classList = "hidden";
    ul.appendChild(li);
}

function saveGameResponseHanlder() {
    let resp = this.response;
    document.getElementById("saves").lastChild.classList.toggle("hidden", false);
}

async function loadGame(e) {
    let timestamp = e.target.innerText;
    let rawData = await getSavesData();
    let saves = await rawData.json();
    for (let save of saves) {
        save = JSON.parse(save);
        if(save.timeStamp === timestamp) {
            let data = save.data;
            let loader = new Saveable(data);
            loader.restore();
            initNoLineState();
            displayNotice("Az állás betöltve.");
        }
    } 
    

}