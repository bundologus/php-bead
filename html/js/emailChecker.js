let email = document.querySelector("[name=\"email\"]");

function validateEmail(e) {
    const regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    let emailParent = e.target.parentElement;
    if(regex.test(e.target.value)) {
        emailParent.classList.toggle("has-success", true);
        emailParent.classList.toggle("has-error", false);
        document.querySelector("button[type=\"submit\"]").removeAttribute("disabled");
    } else {
        emailParent.classList.toggle("has-success", false);
        emailParent.classList.toggle("has-error", true);
        document.querySelector("button[type=\"submit\"]").setAttribute("disabled", "");
    }
}

function validateKey(e) {
    if(e.key === "Enter") {
        validateEmail(e);
    }
}

email.addEventListener("blur", validateEmail);
email.addEventListener("keydown", validateKey);